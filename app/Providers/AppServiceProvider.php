<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade as FacadesBlade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //untuk fungsi rupiah
        FacadesBlade::directive('rupiah', function ($expresion) {
            return "<?= number_format($expresion,2,',','.') ?>";
        });
    }
}
